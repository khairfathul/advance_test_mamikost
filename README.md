# Advance Test Mamikos

### Dependencies
- cucumber-core Version 6.9.1
- cucumber-java Version 6.9.1
- cucumber-junit Version 6.9.1
- cucumber-html Version 0.2.7
- cobertura Version 2.1.1
- cucumber-jvm-deps Version 1.0.6
- cucumber-reporting Version 5.5.2
- hamcrest-core Version 2.2
- gherkin Version 16.0.0
- selenium-java Version 3.141.59
- junit Version 4.13.1
- webdrivermanager Version 3.8.1

### How to Run this Project:
```sh
$ clone this project
$ open maven - reload all maven project
$ open maven - generate source and update folders for all folders
$ open maven - download source
$ go to features file
$ click on run test
```
