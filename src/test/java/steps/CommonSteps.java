package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageObjects.CommonPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;
import utilities.UrlManager;

import java.util.List;


public class CommonSteps {
    private final WebDriver driver = ThreadManager.getDriver();
    private CommonPO common = new CommonPO(driver);
    public static final String PROPERTYFILE="src/test/resources/testData/user.properties";
    static final String user_phone = JavaHelpers.getPropertyValue(PROPERTYFILE,"phone_number_user");
    static final String user_password = JavaHelpers.getPropertyValue(PROPERTYFILE,"password_user");
    static final String owner_phone = JavaHelpers.getPropertyValue(PROPERTYFILE,"phone_number_owner");
    static final String owner_password = JavaHelpers.getPropertyValue(PROPERTYFILE,"password_owner");

    @When("open {string} homepage")
    public void open_web_homepage(String url){
        if(url.equalsIgnoreCase("mamikos"))
        {
            url = UrlManager.MAMIKOS;
            System.out.println("Go to " +url);
        }
        driver.get(url);
    }

    @And("user click login as a tenant")
    public void click_login_as_a_tenant() throws InterruptedException{
        common.clickLoginNav();
        common.clickLoginTenant();
    }

    @And("user click login as an owner")
    public void click_login_as_an_owner() throws InterruptedException{
        common.clickLoginNav();
        common.clickLoginOnwer();
    }


    @And("{string} fill phone number and password")
    public void user_fill_phone_number_and_passwrod(String type)throws InterruptedException{
        if (type == "user"){
            common.inputTenantPhoneNumber(user_phone);
            common.inputTenantPassword(user_password);
            common.clickLoginButton();

        }
        else{
            common.inputTenantPhoneNumber(owner_phone);
            common.inputTenantPassword(owner_password);
            common.clickLoginButton();
            common.clickCloseButton();
        }
    }



}
