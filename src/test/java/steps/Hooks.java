package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import utilities.DriverManager;


public class Hooks
{
    DriverManager drivermanager = new DriverManager();
    @Before
    public void openBrowser()
    {
        drivermanager.openBrowser();
        System.out.println("===================================");
        System.out.println("Start to Open Browser");
    }

    @After
    public void closeBrowser(Scenario scenario)
    {
        if (scenario.isFailed())
        {
            System.out.println("Steps Failed");
        }
        else {
            System.out.println("Notes: All steps has been executed");
        }
        System.out.println("Close Browser");
        System.out.println("===================================");
        drivermanager.closeBrowser();
    }
}
