package steps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageObjects.CancelBookingPO;
import utilities.ThreadManager;

public class CancelBookingSteps {
    private final WebDriver driver = ThreadManager.getDriver();
    private CancelBookingPO cancel = new CancelBookingPO(driver);

    @And("user cancel booking on last booked kost")
    public void user_cancel_booking_on_last_booked_kost() throws InterruptedException {
        cancel.cancelBooking();
    }

    @Then("user validate title kost equals to {string}")
    public void user_validate_kost_name(String title){
        Assert.assertEquals(title, cancel.getTitleTexxt());
    }

}
