package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageObjects.BookingPO;
import utilities.JavaHelpers;
import utilities.ThreadManager;

import java.text.ParseException;

public class BoookingSteps {

    private final WebDriver driver = ThreadManager.getDriver();
    private BookingPO booking = new BookingPO(driver);
    private JavaHelpers java = new JavaHelpers();

    @And("user search area with value {string}")
    public void user_search_area(String area) throws InterruptedException{
        booking.searchArea(area);
    }

    @And("user set filter 24 jam and booking langsung")
    public void user_set_filter_24Jam_and_booking_langsung()throws InterruptedException{
        booking.setFilterSearch();
    }

    @And("user click kost on kost list and close ftue")
    public void user_click_kost_kost_list_and_close_ftue()throws InterruptedException{
        booking.selectKostList();
    }

    @And("user clicks on Booking button on Kost details page")
    public void user_clicks_on_booking_button_on_kost_details_page() throws InterruptedException, ParseException {
        String tomorrow = java.updateTime("yyyy MMM dd", java.getTimeStamp("yyy MMM dd"), "d", 1, 0, 0, 0);
        booking.scrollToKostName();
        booking.selectDateForStartBoarding(tomorrow);
        booking.selectRentType("Per bulan");
        booking.clickOnBookingButton();
    }

    @And("user ajukan sewa")
    public void ajukan_sewa()throws InterruptedException{
        booking.clickCheckBox();
        booking.clickAjukanSewa();
    }


    @And("user click chat pemilik kost")
    public void click_pemilik_kost() throws InterruptedException {
        booking.clickChatPemilikKostButton();
    }

    @Then("user user validate message status equals to {string}")
    public void user_validate_message(String pesan){
        Assert.assertEquals(pesan, booking.getStatus());
    }
}
