package steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import pageObjects.CommonPO;
import pageObjects.OwnerManageBookingPO;
import utilities.JavaHelpers;
import utilities.SeleniumHelpers;
import utilities.ThreadManager;

public class OwnerManageBookingSteps {

    private final WebDriver driver = ThreadManager.getDriver();
    private OwnerManageBookingPO ownerManageBooking = new OwnerManageBookingPO(driver);
    public static final String PROPERTYFILE="src/test/resources/testData/user.properties";
    static final String tenant_name = JavaHelpers.getPropertyValue(PROPERTYFILE,"tenant_name");
    static final String tenant_phoneNumber = JavaHelpers.getPropertyValue(PROPERTYFILE,"tenant_phoneNumber");
    static final String room_name = JavaHelpers.getPropertyValue(PROPERTYFILE,"room_name");
    static final String monthly_price = JavaHelpers.getPropertyValue(PROPERTYFILE,"monthly_price");
    static final String additional_price1 = JavaHelpers.getPropertyValue(PROPERTYFILE,"additional_price1");
    static final String additional_price2 = JavaHelpers.getPropertyValue(PROPERTYFILE,"additional_price2");
    static final String total_price = JavaHelpers.getPropertyValue(PROPERTYFILE,"total_price");


    @And("owner add new tennant")
    public void owner_tambah_penyewa()throws InterruptedException{
        ownerManageBooking.clickTambahPenyewa();
    }

    @And("owner skip onboarding")
    public void skip_onboarding()throws InterruptedException{
        ownerManageBooking.skipOnboarding();
    }

    @And("owner fill tenants phone number with value {string}")
    public void tambah_penyewa(String phone)throws InterruptedException{
        ownerManageBooking.fillTenantData(phone);
    }

    @And("owner add detail penyewa with name {string}")
    public void owner_add_detail_penyewa_with_name(String name)throws InterruptedException{
        ownerManageBooking.fillDetailTenantData(name);
    }

    @And("owner add additional cost 1")
    public void tambah_biaya_tambahan1()throws InterruptedException{
        String jenisBiaya = "biaya tambahan 1";
        String price = "30000";
        ownerManageBooking.inputBiayaTambahan1(jenisBiaya, price);
    }

    @And("owner add additional cost 2")
    public void tambah_biaya_tambahan2()throws InterruptedException{
        String jenisBiaya = "biaya tambahan 2";
        String price = "34000";
        ownerManageBooking.inputBiayaTambahan2(jenisBiaya, price);

    }

    @And("owner add fine with value {string} and end date with value {string}")
    public void tambah_denda(String denda, String batasAkhir)throws InterruptedException{
        ownerManageBooking.fillDenda(denda, batasAkhir);

    }

    @Then("owner verify data")
    public void owner_verify_data_with_value(){
        Assert.assertEquals("value not equals to " +tenant_name,tenant_name, ownerManageBooking.getNamaLengkap());
        Assert.assertEquals("value not equals to " +tenant_phoneNumber,tenant_phoneNumber, ownerManageBooking.getNomorHp());
        Assert.assertEquals("value not equals to " +room_name,room_name, ownerManageBooking.getNomorKamar());
        Assert.assertEquals("value not equals to " +monthly_price,monthly_price, ownerManageBooking.getPriceBulanan());
        Assert.assertEquals("value not equals to " +additional_price1,additional_price1, ownerManageBooking.getTagihan1());
        Assert.assertEquals("value not equals to " +additional_price2,additional_price2, ownerManageBooking.getTagihan2());
        Assert.assertEquals("value not equals to " +total_price,total_price, ownerManageBooking.getTotalPrice());
    }

    @And("owner save data")
    public void owner_save_data()throws InterruptedException{
        ownerManageBooking.simpan_data();
    }

    @And("owner terminate contract")
    public void owner_terminate_contract()throws InterruptedException{
        ownerManageBooking.cancel_order();
    }

    @Then("owner verify termination message is equals to {string}")
    public void owner_verify_termination_contract(String message){
        Assert.assertEquals("",message, ownerManageBooking.getTerminationMessage());
    }

}
