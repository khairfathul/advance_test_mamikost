@testing
Feature: Checkout

  Scenario: Checkout
    Given open "mamikos" homepage
    And user click login as a tenant
    And "user" fill phone number and password
    And user search area with value "jakarta"
    And user set filter 24 jam and booking langsung
    And user click kost on kost list and close ftue
    And user clicks on Booking button on Kost details page
    And user ajukan sewa
    And user click chat pemilik kost
    Then user user validate message status equals to "delivered"
    Then user validate title kost equals to "Kos Bbkquweeee Gambir Jakarta Pusat"
    And user cancel booking on last booked kost
