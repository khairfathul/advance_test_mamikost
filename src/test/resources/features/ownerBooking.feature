@testing
Feature: Checkout

  Scenario: Owner add New Tenant
    Given open "mamikos" homepage
    And user click login as an owner
    And "owner" fill phone number and password
    And owner add new tennant
    And owner skip onboarding
    And owner fill tenants phone number with value "087312312399"
    And owner add detail penyewa with name "fathul"
    And owner add fine with value "300000" and end date with value "30"
    And owner add additional cost 1
    And owner add additional cost 2
    Then owner verify data
    And owner save data
    And owner terminate contract
    Then owner verify termination message is equals to "Penyewa telah selesai mengakhiri kontrak sewa kos"


    


