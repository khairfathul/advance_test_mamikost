package pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

import java.util.List;

public class CommonPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public CommonPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 50), this);
    }

    @FindBy(xpath = "//*[@class='btn btn-primary btn-mamigreen login-button track-login-tenant']")
    private WebElement enterButtonTenant;

    @FindBy (className = "nav-login-button")
    private WebElement loginNavButton;

    @FindBy (className = "login-user-home")
    private WebElement loginUserButton;

    @FindBy (name = "Nomor Handphone")
    private WebElement phoneInputForm;

    @FindBy (name = "Password")
    private WebElement passwordInputForm;

    @FindBy (className = "login-button")
    private WebElement loginButton;

    @FindBy (className = "login-owner-home")
    private WebElement loginOwnerButton;

    @FindBy (className = "icon-close")
    private WebElement closPopUpButton;




    public void clickLoginNav() throws InterruptedException{
        selenium.clickOn(loginNavButton);
    }

    public void clickLoginTenant () throws InterruptedException{
        selenium.clickOn(loginUserButton);
    }

    public void clickLoginOnwer () throws InterruptedException{
        selenium.clickOn(loginOwnerButton);
    }

    public void inputTenantPhoneNumber (String phone) {
        selenium.inputText(phoneInputForm, phone, true);
    }

    public void inputTenantPassword(String password){
        selenium.inputText(passwordInputForm, password, true);
    }

    public void clickLoginButton ()throws InterruptedException{
       selenium.clickOn(loginButton);
       selenium.hardWait(5);
    }

    public void clickCloseButton ()throws InterruptedException{
        selenium.clickOn(closPopUpButton);
//        selenium.hardWait(10);
    }



}
