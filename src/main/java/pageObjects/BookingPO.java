package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

import java.util.List;

public class BookingPO {

    WebDriver driver;
    SeleniumHelpers selenium;

    public BookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
    }

    @FindBy(className = "btn-cta-label")
    private WebElement searchButton;

    @FindBy (xpath = "//input[@type='search']")
    private WebElement searchInputForm;

    @FindBy (className = "results-title")
    private WebElement resultList;

    @FindBy(xpath = "//*[contains(text(), 'Aturan Kos')]")
    private WebElement aturanKostFilterButton;

    @FindBy(xpath = "//div[@class='open btn-group']//span[.='Akses 24 Jam']")
    private WebElement filter24JamCheckBox;

    @FindBy(xpath = "//span[text()='Booking Langsung']")
    private WebElement bookingLangsungButton;

    @FindBy(xpath = "//div[@class='open btn-group']//span[@class='slider round']")
    private WebElement bookingLangsungSlider;

    @FindBy(className = "popper-ftue__content-button")
    private WebElement sayaMengertiButton;

    @FindBy(className = "base-save")
    private List<WebElement> saveButton;

    @FindBy(xpath = "//*[@class='col-12']")
    private List<WebElement> kostList;

    @FindBy(xpath = "//button[@aria-label='Next slide']")
    private WebElement nextButton;

    @FindBy(css = ".booking-card__booking-action")
    private WebElement bookingButton;

    private By dateTextBox = By.xpath("//input[@class='booking-input-checkin__input']");

    @FindBy(css = ".name .--kost-title")
    private WebElement kostNameTitle;

    @FindBy(xpath = "(//span[@class='cell day today'])[2]")
    private WebElement datePickToday;

    @FindBy(xpath = "(//span[@class='cell day'])[1]")
    private WebElement datePickTomorrow;

    @FindBy(id = "mamiCheckboxCommon")
    private WebElement agreementCheckBox;

    @FindBy(className = "track_success_booking")
    private WebElement ajukanSewaButton;

    @FindBy(className = "booking-success__button-chat")
    private WebElement chatPemilkKostButton;

    @FindBy(css = ".message-set.user:last-child .message-item-desc .status")
    private WebElement pesanAtribute;

    public void searchArea(String area)throws InterruptedException{
        selenium.clickOn(searchButton);
        selenium.inputText(searchInputForm,area,true);
        selenium.clickOn(resultList);
    }

    public void setFilterSearch()throws InterruptedException{
        selenium.waitUntilElementToBeClickable(sayaMengertiButton);
        selenium.clickOn(sayaMengertiButton);
        selenium.waitUntilElementToBeClickable(sayaMengertiButton);
        selenium.clickOn(sayaMengertiButton);
        selenium.clickOn(aturanKostFilterButton);
        selenium.javascriptClickOn(filter24JamCheckBox);
        selenium.clickOn(saveButton.get(3));
        selenium.clickOn(bookingLangsungButton);
        selenium.javascriptClickOn(bookingLangsungSlider);
        selenium.clickOn(bookingLangsungButton);
    }

    public void selectKostList()throws InterruptedException{
        selenium.clickOn(kostList.get(0));
        selenium.switchToWindow(0);
        selenium.waitUntilElementToBeClickable(nextButton);
        selenium.clickOn(nextButton);
        selenium.waitUntilElementToBeClickable(nextButton);
        selenium.clickOn(nextButton);
        selenium.waitUntilElementToBeClickable(nextButton);
        selenium.clickOn(nextButton);
        selenium.waitUntilElementToBeClickable(nextButton);
        selenium.clickOn(nextButton);
        selenium.waitUntilElementToBeClickable(nextButton);
        selenium.clickOn(nextButton);
        selenium.waitUntilElementToBeClickable(nextButton);
        selenium.clickOn(nextButton);
        selenium.hardWait(2);
    }

    public void clickOnBookingButton(){
        selenium.pageScrollInView(bookingButton);
        selenium.waitUntilElementToBeClickable(bookingButton);
        selenium.javascriptClickOn(bookingButton);
    }

    public void scrollToKostName() {
        selenium.pageScrollInView(kostNameTitle);
    }

    public void selectDateForStartBoarding(String date) throws InterruptedException {
        if(selenium.waitInCaseElementPresent(dateTextBox,5)!=null){
            if(date.equalsIgnoreCase("today")){
                selenium.javascriptClickOn(dateTextBox);
                selenium.clickOn(datePickToday);
            }else {
                selenium.javascriptClickOn(dateTextBox);
                selenium.javascriptClickOn(datePickTomorrow);
            }
        }
    }

    public void selectRentType(String type) {
        selenium.javascriptClickOn(By.xpath("//div[@class='booking-rent-type__options']/div[@class='booking-rent-type__options-item']/div[contains(.,'"+type+"')]"));
    }

    public void clickCheckBox(){
        selenium.pageScrollInView(ajukanSewaButton);
        selenium.javascriptClickOn(agreementCheckBox);
    }

    public void clickAjukanSewa() throws InterruptedException{
        selenium.clickOn(ajukanSewaButton);
    }

    public void clickChatPemilikKostButton()throws InterruptedException{
        selenium.clickOn(chatPemilkKostButton);
    }

    public String getStatus() {
        selenium.waitTillElementIsVisible(pesanAtribute, 3);
        String element = selenium.getElementAttributeValue(pesanAtribute, "alt");
        return element;
    }
}
