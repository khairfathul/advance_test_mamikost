package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

import java.util.List;

public class OwnerManageBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public OwnerManageBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 50), this);
    }

    @FindBy(xpath = "//p[.='Manajemen Kos']")
    private WebElement managementKosMenu;

    @FindBy(xpath = "//p[.='Penyewa']")
    private WebElement tenantMenu;

    @FindBy(css = ".bg-c-button")
    private WebElement addNewTenantButton;

    @FindBy(css = ".bg-c-button")
    private WebElement lanjutButton;

    @FindBy(css = ".bg-c-button--primary")
    private WebElement lanjutPrimaryButton;

    @FindBy (xpath =  "//span[contains(.,'Saya yang menambah kontrak')]")
    private WebElement ownerCreateContractCard;

    @FindBy (css = ".add-tenant-kost-item__name")
    private WebElement kostList;

    @FindBy (id = "phoneNumberField")
    private WebElement phoneNumberField;

    @FindBy (xpath = "//input[@placeholder='Pilih nomor kamar kos']")
    private WebElement roomNumberField;

    @FindBy (className = "room-name")
    private List<WebElement> roomName;

    @FindBy (xpath = "//button[contains(.,'Terapkan')]")
    private WebElement applyButton;

    @FindBy (xpath = "//div[@class='form-button']//span[.='Tambah Penyewa']")
    private WebElement addTenantInformationButton;

    @FindBy (xpath = "//*[@placeholder='Isi sesuai kartu identitas penyewa']")
    private WebElement tenantNameForm;

    @FindBy (xpath = "//button[contains(.,'Selanjutnya')]")
    private WebElement nextButton;

    @FindBy (xpath = "//span[.='Pilih hitungan sewa']")
    private WebElement contractTypeForm;

    @FindBy (xpath = "//option[@value='monthly']")
    private WebElement monthlyOption;

    @FindBy (xpath = "//span[.='Pilih durasi sewa']")
    private WebElement durationTypeForm;

    @FindBy (xpath = "//option[@value='1']")
    private WebElement monthlyTypeForm;

    @FindBy (xpath = "//*[@placeholder='Pilih tanggal jatuh tempo']")
    private WebElement dueDateForm;

    @FindBy (xpath = "(//span[@class='cell day today'])")
    private WebElement todayDatePicker;

    @FindBy (xpath = "//*[@placeholder='Contoh: Listrik']")
    private List<WebElement> additionalFeeForm;

    @FindBy (xpath = "//input[@min='0']")
    private List<WebElement> priceForm;

    @FindBy (xpath = "//button[contains(.,'Tambah')]")
    private WebElement addButton;

    @FindBy (xpath = "//button[contains(.,'Simpan')]")
    private WebElement saveButton;

    @FindBy (xpath = "//button[contains(.,'Ya, Simpan Data')]")
    private WebElement yesSaveButton;

    @FindBy (className = "add-tenant-info-item__info")
    private List<WebElement> detailDataUserField;

    @FindBy (className = "add-tenant-price-wrapper")
    private List<WebElement> detailPaymentField;

    @FindBy (xpath = "//*[@type='number']")
    private WebElement endDateForm;

    @FindBy (xpath = "//button[contains(.,'Lihat Detail Penyewa')]")
    private WebElement seeDetailsTenantData;

    @FindBy (xpath = "//span[.='Hentikan Kontrak Sewa']")
    private WebElement stopContractButton;

    @FindBy (className = "label-list")
    private List<WebElement> reasonList;

    @FindBy (xpath = "//*[@placeholder='Pilih tanggal']")
    private WebElement dateForm;

    @FindBy (xpath = "//button[contains(.,'Pilih')]")
    private WebElement selectDate;

    @FindBy (xpath = "//button[contains(text(),'Konfirmasi Pemberhentian Sewa')]")
    private WebElement terminateButton;

    @FindBy (className = "termination-card-info__description")
    private WebElement terminantionMessage;

    /**
     * click manageKost menu
     * click tenant submenu
     * click addNewTenant
     */
    public void clickTambahPenyewa ()throws InterruptedException{
        selenium.clickOn(managementKosMenu);
        selenium.clickOn(tenantMenu);
        selenium.clickOn(addNewTenantButton);
    }

    public void skipOnboarding() throws InterruptedException{
        selenium.clickOn(lanjutButton);
        selenium.clickOn(lanjutPrimaryButton);
        selenium.clickOn(lanjutPrimaryButton);
        selenium.clickOn(lanjutPrimaryButton);
    }


    public void fillTenantData(String phone)throws InterruptedException{
        selenium.clickOn(ownerCreateContractCard);
        selenium.clickOn(kostList);
        selenium.inputText(phoneNumberField,phone, true);
        selenium.hardWait(3);
        selenium.doubleClickOnElement(roomNumberField);
        selenium.hardWait(3);
        selenium.clickOn(roomName.get(0));
        selenium.hardWait(3);
        selenium.clickOn(applyButton);
        selenium.clickOn(addTenantInformationButton);
    }

    public void fillDetailTenantData(String name)throws InterruptedException{
        selenium.inputText(tenantNameForm, name, true);
        selenium.clickOn(nextButton);
        selenium.clickOn(contractTypeForm);
        selenium.clickOn(monthlyOption);
        selenium.hardWait(5);
        selenium.clickOn(durationTypeForm);
        selenium.clickOn(monthlyTypeForm);
        selenium.clickOn(dueDateForm);
        selenium.javascriptClickOn(todayDatePicker);
        selenium.clickOn(nextButton);
    }

    public void inputBiayaTambahan1(String jenisBiaya, String price)throws InterruptedException{
        selenium.inputText(additionalFeeForm.get(0), jenisBiaya, true);
        selenium.inputText(priceForm.get(1), price, false);
        selenium.clickOn(addButton);
    }
    public void inputBiayaTambahan2(String jenisBiaya, String price)throws InterruptedException{
        selenium.inputText(additionalFeeForm.get(1), jenisBiaya, true);
        selenium.inputText(priceForm.get(2), price, false);
        selenium.clickOn(nextButton);

    }

    public String getNamaLengkap(){
        return selenium.getText(detailDataUserField.get(0));
    }
    public String getNomorHp(){
        return selenium.getText(detailDataUserField.get(1));
    }

    public String getNomorKamar(){
        return selenium.getText(detailDataUserField.get(2));
    }

    public String getPriceBulanan(){
        return selenium.getText(detailPaymentField.get(0));
    }
    public String getTagihan1(){
        return selenium.getText(detailPaymentField.get(1));
    }

    public String getTagihan2(){
        return selenium.getText(detailPaymentField.get(2));
    }

    public String getTotalPrice(){
        return selenium.getText(detailPaymentField.get(3));
    }

    public void simpan_data()throws InterruptedException{
        selenium.clickOn(saveButton);
        selenium.clickOn(yesSaveButton);
    }

    public void fillDenda(String denda, String batasAkhir){

        selenium.inputText(priceForm.get(0), denda, false);
        selenium.inputText(endDateForm, batasAkhir, true);
    }

    public void cancel_order()throws InterruptedException{
        selenium.clickOn(seeDetailsTenantData);
        selenium.hardWait(3);
        selenium.clickOn(stopContractButton);
        selenium.clickOn(reasonList.get(0));
        selenium.clickOn(dateForm);
        selenium.clickOn(todayDatePicker);
        selenium.clickOn(selectDate);
        selenium.clickOn(terminateButton);
    }

    public String getTerminationMessage(){
        return selenium.getText(terminantionMessage);
    }
}
