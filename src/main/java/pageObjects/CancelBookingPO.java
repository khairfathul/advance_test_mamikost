package pageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import utilities.SeleniumHelpers;

import java.util.List;

public class CancelBookingPO {
    WebDriver driver;
    SeleniumHelpers selenium;

    public CancelBookingPO(WebDriver driver) {
        this.driver = driver;
        selenium = new SeleniumHelpers(driver);

        // This initElements method will create all WebElements
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 30), this);
    }

    @FindBy(xpath = "//*[contains(text(), 'Lihat selengkapnya')]")
    private List<WebElement> lihatSelengkapnyaButton;

    @FindBy(xpath = "//div[@class='detail-cancel']/button[@class='btn btn-success']")
    private WebElement batalkanBooking;

    @FindBy(xpath = "//*[contains(text(), 'Ya, Batalkan')]")
    private List<WebElement> yaBatalkanButton;

    @FindBy(className = "ic-close")
    private WebElement closeChatButton;

    @FindBy(className = "ic-minimize")
    private WebElement minimizeChatButton;

    @FindBy (className = "kost-title")
    private WebElement kostTitleText;

    public void cancelBooking()throws InterruptedException{
        selenium.clickOn(closeChatButton);
        selenium.clickOn(minimizeChatButton);
        selenium.hardWait(5);
        selenium.clickOn(lihatSelengkapnyaButton.get(0));
        selenium.scrollHalf();
        selenium.hardWait(5);
        selenium.clickOn(batalkanBooking);
        selenium.javascriptClickOn(yaBatalkanButton.get(1));
    }

    public String getTitleTexxt(){
        selenium.waitTillElementIsVisible(kostTitleText);
        return selenium.getText(kostTitleText);
    }
}
