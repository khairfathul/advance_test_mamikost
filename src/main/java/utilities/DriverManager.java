package utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverManager {
    public void openBrowser()
    {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--incognito");
        options.addArguments("start-maximized");
        ThreadManager.setDriver(new ChromeDriver(options));
    }

    public void closeBrowser()
    {

        if(ThreadManager.getDriver()!=null)
        {
            ThreadManager.getDriver().quit();
        }
    }
}
