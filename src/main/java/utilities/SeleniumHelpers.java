package utilities;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import java.util.*;

public class SeleniumHelpers {
    protected WebDriver driver;
    protected Actions actions;
    protected WebDriverWait wait;

    public SeleniumHelpers(WebDriver driver)
    {
        this.driver = driver;
        actions = new Actions(driver);
        wait = new WebDriverWait(driver, 30);
    }

    public void clickOn(WebElement e) throws InterruptedException
    {
        waitUntilElementToBeClickable(e).click();
        waitForJavascriptToLoad();
    }

    public void inputText (WebElement element, String text, boolean clear) {
        element = waitUntilElementToBeClickable(element);
        if(clear)
        {
            element.clear();
        }
        element.sendKeys(text);
    }

    public String getText(WebElement e)
    {
        return waitTillElementIsVisible(e).getText().trim();
    }


    public WebElement waitUntilElementToBeClickable (WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        return element;
    }


    public WebElement waitTillElementIsVisible(WebElement element, int waitDurationInSeconds)
    {
        WebDriverWait wait = new WebDriverWait(driver, waitDurationInSeconds);
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public  void waitForJavascriptToLoad() throws InterruptedException
    {
        Thread.sleep(1000);
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
            }
        };
        Wait<WebDriver> wait = new WebDriverWait(driver, 50);
        try
        {
            wait.until(expectation);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        catch(Error e)
        {
            e.printStackTrace();
        }
    }

    public WebElement pageScrollInView(WebElement e)
    {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].scrollIntoView(true);",e);
        return e;
    }

    public void scrollHalf(){
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollBy(0,950)");
    }

    public void hardWait(int seconds) throws InterruptedException
    {
        Thread.sleep(seconds * 1000);
    }

    public void switchToWindow(int tabNumber)
    {
        int i = 1;
        for (String winHandle : getWindowHandles())
        {
            driver.switchTo().window(winHandle);
            if (i == tabNumber)
                break;
            i++;
        }
    }

    public Set<String> getWindowHandles()
    {
        return driver.getWindowHandles();
    }

    public void javascriptClickOn(WebElement e) {
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", e);
    }

    public WebElement waitTillElementIsVisible(WebElement e)
    {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        wait.until(ExpectedConditions.visibilityOf(e));
        return e;
    }

    public WebElement waitInCaseElementPresent(By e, int duration) {
        WebDriverWait wait = new WebDriverWait(driver, duration);
        try{
            return wait.until(ExpectedConditions.presenceOfElementLocated(e));
        }
        catch (Exception ex) {
            return null;
        }
    }

    public void javascriptClickOn(By by) {
        WebElement element = driver.findElement(by);
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", element);
    }

    public String getElementAttributeValue(WebElement e, String attributeName)
    {
        if(isElementAtrributePresent(e,attributeName))
        {
            return e.getAttribute(attributeName);
        }
        return "Attribute" + attributeName +" not found";
    }

    public boolean isElementAtrributePresent(WebElement e, String attributeName)
    {
        return e.getAttribute(attributeName) != null;
    }

    public void doubleClickOnElement(WebElement e)
    {
        actions.doubleClick(e).build().perform();
    }



}
