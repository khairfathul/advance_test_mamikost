package utilities;

import org.openqa.selenium.WebDriver;

public class ThreadManager {
    private static ThreadLocal<WebDriver> webDriver = new ThreadLocal();

    public static synchronized  void setDriver(WebDriver driver) {
        webDriver.set(driver);
    }

    public static synchronized WebDriver getDriver() {
        return webDriver.get();
    }
}
